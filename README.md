# go-webapp

Init

```
go mod webapp
```

Hello world:

```
package main

import (
  "net/http"

  "github.com/gin-gonic/gin"
)

func main() {
  r := gin.Default()
  r.GET("/ping", func(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{
      "message": "pong",
    })
  })
  r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
```

Get gin

```
 go get -u github.com/gin-gonic/gin
```

Run on `0.0.0.0:8080/ping`

```
go run .
```

# Rest client

    https://chromewebstore.google.com/detail/yet-another-rest-client/ehafadccdcdedbhcbddihehiodgcddpl

https://addons.mozilla.org/en-US/firefox/addon/rested/

# References

https://gin-gonic.com/docs/

https://go.dev/doc/tutorial/web-service-gin
